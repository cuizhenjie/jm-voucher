package com.jimei.common;

/**
 * 封装统一code
 * @author czj
 *
 */
public enum ResponseCode {

    SUCCESS(0, "SUCCESS"),
    NOTFOUND(404, "Not Found"),
    BADREQUEST(400, "Bad Request"),
    FORBIDDEN(403, "forbidden"),
    UNAUTHORIZED(401, "unauthorized"),
    SERVERINTERNLERROR(500, "serverInternalError"),
    ERROR(4001, "业务异常");

    private final int code;
    private final String desc;


    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}