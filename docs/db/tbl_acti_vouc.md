
#### 活动派发券详情(tbl_acti_vouc)

| 字段名 | 必填 | 类型 | 描述|
| :--- | :--: | ---: | :--- |
|id|Y|bigint|主键|
|pid|Y|varchar(32)|业务主键|
|activity_pid|Y|varchar(32)|活动PID|
|voucher_type|N|varchar(50)|券种|
|name|Y|varchar(50)|券名称|
|category|N|int|使用品类,0全部,1家具,2建材|
|start_time|Y|datetime|券有效期开始时间|
|end_time|Y|datetime|券有效期结束时间|
|single_num|Y|int|单次派发数量|
|create_time|Y|datetime|创建时间|
|update_time|N|datetime|修改时间|
|operator|Y|varchar(32)|操作人，用户PID|
|status|Y|int|逻辑删除标记，0正常，-1删除|
|version|Y|int||



