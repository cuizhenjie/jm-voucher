#### 活动（tbl_activity）

| 字段名 | 必填 | 类型 | 描述|
| :--- | :--: | ---: | :--- |
|id|Y|bigint|主键|
|pid|Y|varchar(32)|业务主键|
|name|Y|varchar(50)|活动名称|
|img|N|varchar(255)|图片,取banner第一张|
|banner|N|varchar(1024)|banner,','分隔|
|intro|N|text|活动介绍|
|shop|N|varchar(255)|参与门店|
|amount|Y|int|价格(分)|
|type|Y|int|活动类型 1正常2不需要调用erp3不需要支付|
|voucher_mode|Y|int|券类型,1代金券;2储值卡;3资格券|
|count|Y|int|总派发套数|
|rebuy|Y|int|是否可重复购买0不可1可|
|start_time|Y|datetime|活动开始时间|
|end_time|Y|datetime|活动结束时间|
|create_time|Y|datetime|创建时间|
|update_time|N|datetime|修改时间|
|operator|Y|varchar(32)|操作人，用户PID|
|sort|Y|int|排序,默认0|
|status|Y|int|状态, -1删除；0初始化\|1有效\|2失效|
|version|Y|int|乐观锁|

