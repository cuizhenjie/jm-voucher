#### 支付记录流水（tbl_payment）

| 字段名 | 必填 | 类型 | 描述|
| :--- | :--: | ---: | :--- |
|id|Y|bigint|主键|
|pid|Y|varchar(32)|业务主键|
|order_pid|Y|varchar(32)|订单pid,订单号|
|pay_type|Y|int|支付方式，0微信；1支付宝|
|pay_amount|Y|int|支付金额|
|pay_status|Y|int|支付状态,0未支付；-1已取消；1已支付|
|pay_time|N|datetime|支付时间|
|deal_code|N|varchar(50)|支付流水号|
|create_time|Y|datetime|创建时间|
|update_time|Y|datetime|修改时间|
|status|Y|int|逻辑删除标记，0正常，-1删除|
|version|Y|int||


