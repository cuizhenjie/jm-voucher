
#### 电子券（tbl_voucher）

| 字段名 | 必填 | 类型 | 描述|
| :--- | :--: | ---: | :--- |
| id | Y | bigint |主键|
|pid|Y|varchar(32)|业务主键|
|code| Y | bigint |16位数字券码|
|pass|Y| varchar(18)|券秘钥|
|activity_pid|Y|varchar(32)|活动PID|
|acti_list_pid|Y|varchar(32)|活动场次ID|
|acti_vouc_pid|Y|varchar(32)|活动派发券详情ID|
|voucher_mode|Y|int|券类型,1代金券;2储值卡;3资格券|
|voucher_type|N|varchar(50)|券种|
|voucher_status|Y|int|券状态,0未派发；1已派发,2核销|
|order_pid|N|varchar(32)|订单PID,订单号|
|create_time|Y|datetime|创建时间|
|update_time|Y|datetime|修改时间|
|status|Y|int|逻辑删除标记，0正常，-1删除|
|version|Y|int||


