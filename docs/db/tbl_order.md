
#### 订单（tbl_order）

| 字段名 | 必填 | 类型 | 描述|
| :--- | :--: | ---: | :--- |
|id|Y|bigint|主键|
|pid|Y|varchar(32)|业务主键,订单号|
|activity_pid|Y|varchar(32)|活动PID|
|acti_list_pid|Y|varchar(32)|活动场次PID|
|user_pid|Y|varchar(32)|用户PID|
|phone|Y|varchar(16)|用户手机号|
|pay_type|Y|int|支付方式，0微信；1支付宝|
|pay_amount|Y|int|应付金额|
|discount_amount|Y|int|优惠金额|
|total_amount|Y|int|订单总额|
|pay_time|N|datetime|支付时间|
|deal_code|N|varchar(50)|支付流水号|
|order_status|Y|int|订单状态,0未支付；-1已取消；1已支付；8已完成|
|create_time|Y|datetime|下单时间|
|update_time|Y|datetime|修改时间|
|status|Y|int|逻辑删除标记，0正常，-1删除|
|version|Y|int||




