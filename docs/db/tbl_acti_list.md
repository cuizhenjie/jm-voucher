
#### 活动场次（tbl_acti_list）

| 字段名 | 必填 | 类型 | 描述|
| :--- | :--: | ---: | :--- |
|id|Y|bigint|主键|
|pid|Y|varchar(32)|业务主键|
|activity_pid|Y|varchar(32)|活动PID|
|name|Y|varchar(50)|场次名称|
|count|Y|int|单场派发套数|
|start_time|Y|datetime|场次开始时间|
|end_time|Y|datetime|场次结束时间|
|create_time|Y|datetime|创建时间|
|update_time|N|datetime|修改时间|
|operator|Y|varchar(32)|操作人，用户PID|
|status|Y|int|逻辑删除标记，0正常，-1删除|
|version|Y|int||


