-- MySQL dump 10.13  Distrib 5.7.23, for Win64 (x86_64)
--
-- Host: localhost    Database: voucher
-- ------------------------------------------------------
-- Server version	5.7.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_acti_list`
--

DROP TABLE IF EXISTS `tbl_acti_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_acti_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL COMMENT '业务主键',
  `activity_pid` varchar(32) NOT NULL COMMENT '活动PID',
  `name` varchar(50) NOT NULL COMMENT '场次名称',
  `count` int(11) NOT NULL COMMENT '单场派发套数',
  `start_time` datetime NOT NULL COMMENT '场次开始时间',
  `end_time` datetime NOT NULL COMMENT '场次结束时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `operator` varchar(32) NOT NULL COMMENT '操作人，用户PID',
  `status` int(11) NOT NULL COMMENT '逻辑删除标记，0正常，-1删除\n\n\n',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_acti_vouc`
--

DROP TABLE IF EXISTS `tbl_acti_vouc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_acti_vouc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL COMMENT '业务主键',
  `activity_pid` varchar(32) NOT NULL COMMENT '活动PID',
  `voucher_type` varchar(50) DEFAULT NULL COMMENT '券种',
  `name` varchar(50) NOT NULL COMMENT '券名称',
  `category` int(11) DEFAULT NULL COMMENT '使用品类,0全部,1家具,2建材',
  `start_time` datetime NOT NULL COMMENT '券有效期开始时间',
  `end_time` datetime NOT NULL COMMENT '券有效期结束时间',
  `single_num` int(11) NOT NULL COMMENT '单次派发数量',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `operator` varchar(32) NOT NULL COMMENT '操作人，用户PID',
  `status` int(11) NOT NULL COMMENT '逻辑删除标记，0正常，-1删除',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_activity`
--

DROP TABLE IF EXISTS `tbl_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL COMMENT '业务主键',
  `name` varchar(50) NOT NULL COMMENT '活动名称',
  `img` varchar(255) DEFAULT NULL COMMENT '图片,取banner第一张',
  `banner` varchar(1024) DEFAULT NULL COMMENT 'banner,'',''分隔',
  `intro` text COMMENT '活动介绍',
  `shop` varchar(255) DEFAULT NULL COMMENT '参与门店',
  `amount` int(11) NOT NULL COMMENT '价格(分)',
  `type` int(11) NOT NULL COMMENT '活动类型 1正常2不需要调用erp3不需要支付',
  `voucher_mode` int(11) NOT NULL COMMENT '券类型,1代金券;2储值卡;3资格券',
  `count` int(11) NOT NULL COMMENT '总派发套数',
  `rebuy` int(11) NOT NULL COMMENT '是否可重复购买0不可1可',
  `start_time` datetime NOT NULL COMMENT '活动开始时间',
  `end_time` datetime NOT NULL COMMENT '活动结束时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `operator` varchar(32) NOT NULL COMMENT '操作人，用户PID',
  `sort` int(11) NOT NULL COMMENT '排序,默认0',
  `status` int(11) NOT NULL COMMENT '状态, -1删除；0初始化\\|1有效\\|2失效',
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_order`
--

DROP TABLE IF EXISTS `tbl_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL COMMENT '业务主键,订单号',
  `activity_pid` varchar(32) NOT NULL COMMENT '活动PID',
  `acti_list_pid` varchar(32) NOT NULL COMMENT '活动场次PID',
  `user_pid` varchar(32) NOT NULL COMMENT '用户PID',
  `phone` varchar(16) NOT NULL COMMENT '用户手机号',
  `pay_type` int(11) NOT NULL COMMENT '支付方式，0微信；1支付宝',
  `pay_amount` int(11) NOT NULL COMMENT '应付金额',
  `discount_amount` int(11) NOT NULL COMMENT '优惠金额',
  `total_amount` int(11) NOT NULL COMMENT '订单总额',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `deal_code` varchar(50) DEFAULT NULL COMMENT '支付流水号',
  `order_status` int(11) NOT NULL COMMENT '订单状态,0未支付；-1已取消；1已支付；8已完成',
  `create_time` datetime NOT NULL COMMENT '下单时间',
  `update_time` datetime DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '逻辑删除标记，0正常，-1删除',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_payment`
--

DROP TABLE IF EXISTS `tbl_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL,
  `order_pid` varchar(32) NOT NULL COMMENT '订单PID，订单号',
  `pay_type` int(11) NOT NULL COMMENT '支付方式，0微信；1支付宝',
  `pay_amount` int(11) NOT NULL COMMENT '支付金额',
  `pay_status` int(11) NOT NULL COMMENT '支付状态,0未支付；-1已取消；1已支付',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `deal_code` varchar(50) DEFAULT NULL COMMENT '支付流水',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '逻辑删除标记，0正常，-1删除',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_voucher`
--

DROP TABLE IF EXISTS `tbl_voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_voucher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL COMMENT '业务主键',
  `code` bigint(20) NOT NULL COMMENT '券码',
  `pass` varchar(18) NOT NULL COMMENT '券秘钥',
  `activity_pid` varchar(32) NOT NULL COMMENT ' 活动PID',
  `acti_list_pid` varchar(32) NOT NULL COMMENT '活动场次pid',
  `acti_vouc_pid` varchar(32) NOT NULL COMMENT '活动关联券pid',
  `voucher_mode` int(11) NOT NULL COMMENT '券类型1代金券;2储值卡;3资格券',
  `voucher_type` varchar(12) DEFAULT NULL COMMENT '券种',
  `voucher_status` int(11) NOT NULL COMMENT '券状态,0未派发；1已派发,2核销',
  `order_pid` varchar(32) DEFAULT NULL COMMENT '订单PID,订单号',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '逻辑删除标记，0正常，-1删除',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-07 13:54:33
