#电子券接口说明

[TOC]

---

## 1.电子券列表

### 接口功能

> 获取电子券列表

### URL

> [backend/vouchers/list/{activityPid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|page|N|int|第几页,默认返回第一页|
|pageSize|N|int|每页的数量,默认10|
|code|N|long|券号|
|orderPid|N|String|订单号|
|startTime|N|datetime|筛选时间|
|endTime|N|datetime|筛选时间|

### 响应参数

##### data.list参数详情

|参数名称|必填|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|voucherMode|Y|int|券类型,1代金券;2储值卡;3资格券|
|voucherType|Y|String|券种|
|code|Y|long|16位数字券码|
|pass|Y|String|券秘钥|
|voucherStatus|Y|int|券状态,0未派发；1已派发,2已核销|
|createTime|N|datetime|创建时间|
|updateTime|N|datetime|修改时间|
|orderPid|Y|String|订单号|

### 接口返回示例

```json
{
    "code":0,
    "data":
        {
            "total": 1,
            "pageNum": 1,
            "pageSize": 10,
            "pages": 1,
            "size": 1,
            "List":[
                {
                    "pid":"",
                    "voucherMode":1,
                    "voucherType":"",
                    "code":12321321321321321321321321,
                    "pass":"",
                    "voucherStatus":1,
                    "createTime":"2018-05-22 10:05:05",
                    "updateTime":"2018-05-22 10:05:05",
                    "orderCode":""
                }
            ]
        }
}
```


---
## 2.查询

### 接口功能

> 查询电子券

### URL

> [backend/vouchers/view]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|voucherCode|Y|long|16位数字券码|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|data|Y|String|返回数据，见''data参数详情''|

#### data参数详情

|参数名称|必填|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|voucherMode|Y|int|券类型,1代金券;2储值卡;3资格券|
|voucherType|Y|String|券种|
|code|Y|long|16位数字券码|
|pass|Y|String|券秘钥|
|category|N|int|使用品类,0全部,1家具,2建材|
|voucherStatus|Y|int|券状态,0未派发；1已派发,2已核销|
|createTime|N|datetime|创建时间|
|updateTime|N|datetime|修改时间|
|orderPid|Y|String|订单号|


### 接口返回示例

```json
{
    "code":0,
    "data":{
        "pid":"",
        "voucherMode":0,
        "voucherType":"",
        "code":"",
        "pass":"",
        "category":"",
        "voucherStatus":1,
        "createTime":"2018-05-26 10:05:05",
        "updateTime":"2018-05-26 10:05:05",
        "orderPid":""
    }
}
```

---
## 3.核销

### 接口功能

> 核销电子券

### URL

> [backend/vouchers/use]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|voucherPid|Y|String|券ID|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"1231231231231231231231核销成功"
}
```


