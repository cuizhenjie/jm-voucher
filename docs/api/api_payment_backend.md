#支付流水接口说明

[TOC]

---

## 1.支付流水列表

### 接口功能

> 获取支付流水列表

### URL

> [backend/payments/list]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|page|N|int|第几页,默认返回第一页|
|pageSize|N|int|每页的数量,默认10|
|orderPid|N|String|订单号|

### 响应参数

##### data.list参数详情

|参数名称|必填|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|orderPid|Y|String|订单号|
|payType|Y|int|支付方式，0微信；1支付宝|
|payAmount|Y|int|应付金额|
|payStatus|Y|int|支付状态,0未支付；-1已取消；1已支付|
|createTime|Y|datetime|创建时间|
|payTime|N|datetime|支付时间|
|dealCode|N|String|支付流水号|

### 接口返回示例

```json
{
    "code":0,
    "data":
        {
            "total": 1,
            "pageNum": 1,
            "pageSize": 10,
            "pages": 1,
            "size": 1,
            "List":[
                {
                    "pid":"",
                    "orderPid":"",
                    "payType":1,
                    "payAmount":10,
                    "payStatus":0,
                    "createTime":"2018-05-22 10:05:05",
                    "payTime":"2018-05-22 10:05:05",
                    "dealCode":""
                }
            ]
        }
}
```

