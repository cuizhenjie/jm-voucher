#活动接口说明

[TOC]

---

## 1.列表

### 接口功能

> 获取活动列表

### URL

> [backend/activities/list]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|page|N|int|第几页,默认返回第一页|
|pageSize|N|int|每页的数量,默认10|
|name|N|String|活动名称|
|startTime|N|datetime|开始时间|
|endTime|N|datetime|结束时间|
|status|N|int|状态, -1删除；0初始化\|1有效\|2失效|
|type|N|int|活动类型1正常2不需要调用erp3不需要支付|
|voucherMode|N|int|券类型,1代金券;2储值卡;3资格券|

### 响应参数

##### data.list参数详情

|参数名称|必选|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|name|Y|String|活动名称|
|img|Y|String|主图地址|
|amount|Y|int|价格（分）|
|type|Y|int|活动类型1正常2不需要调用erp3不需要支付|
|voucherMode|Y|int|券类型,1代金券;2储值卡;3资格券|
|count|Y|int|总派发套数|
|rebuy|Y|int|是否可重复购买0不可1可|
|startTime|Y|datetime|活动开始时间|
|endTime|Y|datetime|活动结束时间|
|createTime|Y|datetime|创建时间|
|updateTime|Y|datetime|修改时间|
|sort|Y|int|排序|
|status|Y|int|状态, -1删除；0初始化\|1有效\|2失效|

### 接口返回示例

```json
{
    "code":0,
    "data":
        {
            "total": 1,
            "pageNum": 1,
            "pageSize": 10,
            "pages": 1,
            "size": 1,
            "List":[
                {
                    "pid":"",
                    "name":"",
                    "amount":10,
                    "type":1,
                    "voucherMode":1,
                    "count":50,
                    "rebuy":1,
                    "startTime":"2018-05-22 10:05:05",
                    "endTime":"2018-05-26 10:05:05",
                    "createTime":"2018-05-26 10:05:05",
                    "updateTime":"2018-05-26 10:05:05",
                    "sort":1,
                    "status":1
                }
            ]
        }
}
```

---
## 2.新增

### 接口功能

> 新增活动

### URL

> [backend/activities/add]

### 支持格式

> JSON

### HTTP请求方式

> POST

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|name|Y|String|活动名称|
|banner|N|String|banner图片地址,','分隔|
|intro|N|String|活动介绍|
|shop|N|String|参与门店|
|amount|Y|int|价格(分)|
|type|Y|int|活动类型 1正常2不需要调用erp3不需要支付|
|voucherMode|Y|int|券类型,1代金券;2储值卡;3资格券|
|count|Y|int|总派发套数|
|rebuy|Y|int|是否可重复购买0不可1可|
|start_time|Y|datetime|活动开始时间|
|end_time|Y|datetime|活动结束时间|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"新增成功"
}
```

---
## 3.编辑

### 接口功能

> 编辑修改活动

### URL

> [backend/activities/edit]

### 支持格式

> JSON

### HTTP请求方式

> POST

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|name|Y|String|活动名称|
|banner|N|String|banner图片地址,','分隔|
|intro|N|String|活动介绍|
|shop|N|String|参与门店|
|amount|Y|int|价格(分)|
|type|Y|int|活动类型 1正常2不需要调用erp3不需要支付|
|voucherMode|Y|int|券类型,1代金券;2储值卡;3资格券|
|count|Y|int|总派发套数|
|rebuy|Y|int|是否可重复购买0不可1可|
|startTime|Y|datetime|活动开始时间|
|endTime|Y|datetime|活动结束时间|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"修改成功"
}
```

---
## 4.查看

### 接口功能

> 查看活动详情

### URL

> [backend/activities/detail/{pid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

无

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|data|Y|String|返回数据，见''data参数详情''|

#### data参数详情

|参数名称|必选|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|name|Y|String|活动名称|
|banner|N|String|banner,','分隔|
|intro|N|String|活动介绍|
|shop|N|String|参与门店|
|amount|Y|int|价格（分）|
|type|Y|int|活动类型1正常2不需要调用erp3不需要支付|
|voucherMode|Y|int|券类型,1代金券;2储值卡;3资格券|
|count|Y|int|总派发套数|
|rebuy|Y|int|是否可重复购买0不可1可|
|startTime|Y|datetime|活动开始时间|
|endTime|Y|datetime|活动结束时间|
|createTime|Y|datetime|创建时间|
|updateTime|Y|datetime|修改时间|
|sort|Y|int|排序|
|status|Y|int|状态, -1删除；0初始化\|1有效\|2失效|

### 接口返回示例

```json
{
    "code":0,
    "data":{
        "pid":"",
        "name":"",
        "img":"",
        "banner":"",
        "intro":"",
        "shop":"",
        "amount":10,
        "type":1,
        "voucherMode":1,
        "count":50,
        "rebuy":1,
        "startTime":"2018-05-22 10:05:05",
        "endTime":"2018-05-26 10:05:05",
        "createTime":"2018-05-26 10:05:05",
        "updateTime":"2018-05-26 10:05:05",
        "sort":1,
        "status":1
    }
}
```

---
## 5.删除

### 接口功能

> 删除活动

### URL

> [backend/activities/delete/{pid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

无

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"删除成功"
}
```



---
## 6.置顶

### 接口功能

> 置顶活动

### URL

> [backend/activities/top/{pid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

无

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"置顶成功"
}
```