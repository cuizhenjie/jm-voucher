#订单接口说明

[TOC]

---

## 1.订单列表

### 接口功能

> 获取订单列表

### URL

> [backend/orders/list]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|page|N|int|第几页,默认返回第一页|
|pageSize|N|int|每页的数量,默认10|
|activityPid|N|String|活动PID,下拉框选择,下拉框带模糊检索|
|phone|N|String|手机号|
|voucherCode|N|String|券号|
|startTime|N|datetime|筛选开始时间|
|endTime|N|datetime|筛选结束时间|


### 响应参数


##### data.list参数详情

|参数名称|必填|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID,订单号|
|phone|Y|String|手机号|
|createTime|Y|datetime|下单时间|
|payType|Y|int|支付方式，0微信；1支付宝|
|payAmount|Y|int|应付金额|
|discountAmount|Y|int|优惠金额|
|totalAmount|Y|int|订单总额|
|orderStatus|Y|int|订单状态,0未支付；-1已取消；1已支付；8已完成|
|payTime|N|datetime|支付时间|
|dealCode|N|String|支付流水号|

### 接口返回示例

```json
{
    "code":0,
    "data":
        {
            "total": 1,
            "pageNum": 1,
            "pageSize": 10,
            "pages": 1,
            "size": 1,
            "List":[
                {
                    "pid":"",
                    "phone":"",
                    "createTime":"2018-05-22 10:05:05",
                    "payType":1,
                    "payAmount":10,
                    "discountAmount":0,
                    "totalAmount":10,
                    "orderStatus":0,
                    "payTime":"",
                    "dealCode":""
                }
            ]
        }
}
```

---
## 2.查看

### 接口功能

> 查看订单详情

### URL

> [backend/orders/detail/{pid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

无

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|data|N|String|返回数据,见"data参数详情"|
|msg|N|String|失败时返回信息|

#### data参数详情

|参数名称|必填|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID,订单号|
|phone|Y|String|手机号|
|activityName|Y|String|活动名称|
|actiListName|Y|String|活动场次名称|
|userName|Y|String|用户名称|
|createTime|Y|datetime|下单时间|
|payType|Y|int|支付方式，0微信；1支付宝|
|payAmount|Y|int|应付金额|
|discountAmount|Y|int|优惠金额|
|totalAmount|Y|int|订单总额|
|orderStatus|Y|int|订单状态,0未支付；-1已取消；1已支付；8已完成|
|payTime|N|datetime|支付时间|
|dealCode|N|String|支付流水号|

### 接口返回示例

```json
{
    "code":0,
    "data":{
        "pid":"",
        "activityName":"",
        "actiListName":"",
        "userName":"",
        "phone":"",
        "createTime":"2018-05-22 10:05:05",
        "payType":1,
        "payAmount":10,
        "discountAmount":0,
        "totalAmount":10,
        "orderStatus":0,
        "payTime":"",
        "dealCode":""
    }
}
```


---
## 3.补发通知

### 接口功能

> 订单补发通知

### URL

> [backend/orders/notify/{pid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

无

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"发送成功"
}
```

