#活动场次接口说明

[TOC]

---

## 1.列表

### 接口功能

> 获取活动场次列表

### URL

> [backend/acti_lists/{activityPid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|startTime|Y|datetime|筛选开始时间|
|endTime|Y|datetime|筛选结束时间|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|data|N|array|成功时返回,数据集合|
|msg|N|String|失败时返回信息|

####  data参数详情

|参数名称|必填|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|activityPid|Y|String|活动ID|
|name|Y|String|场次名称|
|count|Y|int|单场派发套数|
|startTime|Y|datetime|场次开始时间|
|endTime|Y|datetime|场次结束时间|
|createTime|Y|datetime|创建时间|
|updateTime|N|datetime|修改时间|

### 接口返回示例

```json
{
    "code":0,
    "data":[
        {
            "pid":"",
            "activityPid":"",
            "name":"",
            "count":50,
            "startTime":"",
            "endTime":"",
            "createTime":"",
            "updateTime":""
        },{
            ...
        }
    ]
}
```



---
## 2.新增

### 接口功能

> 新增活动场次

### URL

> [backend/acti_lists/add]

### 支持格式

> JSON

### HTTP请求方式

> POST

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|activityPid|Y|String|活动ID|
|name|Y|String|场次名称|
|count|Y|int|单场派发套数|
|startTime|Y|datetime|场次开始时间|
|endTime|Y|datetime|场次结束时间|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"新增成功"
}
```

---
## 3.编辑

### 接口功能

> 编辑修改活动场次

### URL

> [backend/acti_lists/edit]

### 支持格式

> JSON

### HTTP请求方式

> POST

### 请求参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|pid|Y|String|业务ID|
|name|Y|String|场次名称|
|count|Y|int|单场派发套数|
|startTime|Y|datetime|场次开始时间|
|endTime|Y|datetime|场次结束时间|

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"修改成功"
}
```

---
## 4.删除

### 接口功能

> 删除活动场次

### URL

> [backend/acti_lists/delete/{pid}]

### 支持格式

> JSON

### HTTP请求方式

> GET

### 请求参数

无

### 响应参数

|参数|必选|类型|说明|
|:--|:--|:--|:--|
|code|Y|int|返回代码，0表示成功|
|msg|Y|String|返回信息|

### 接口返回示例

```json
{
    "code":0,
    "msg":"删除成功"
}
```