package com.jimei.voucher.dao;

import java.util.List;
import java.util.Map;

import com.jimei.voucher.pojo.Order;
import com.jimei.voucher.vo.OrderVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public interface OrderMapper {

    /**
    * 列表数据
    * @param params
    */
    List<OrderVO> selectListByParams(Map<String, Object> params);

    /**
    * 通过pid 获取数据
    * @param pid
    */
    Order selectByPid(String pid);

    /**
    * 新增(非空判断)
    * @param order
    */
    int insertSelective(Order order);

    /**
    * 通过pid修改（非空判断）
    * @param order
    */
    int updateByPidSelective(Order order);

    /**
    * 通过pid逻辑删除
    * @param pids
    */
    int deleteByPidSelective(List<String> pids);

    /**
     * 取消订单
     * @param pid
     * @return
     */
    int cancelByPid(String pid);
}
