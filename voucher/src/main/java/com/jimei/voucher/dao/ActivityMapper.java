package com.jimei.voucher.dao;

import java.util.Map;
import java.util.List;

import com.jimei.voucher.pojo.Activity;
import com.jimei.voucher.vo.ActivityVO;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public interface ActivityMapper {

    /**
    * 列表数据
    * @param params
    */
    List<Activity> selectListByParams(Map<String, Object> params);

    /**
    * 通过pid 获取数据
    * @param pid
    */
    Activity selectByPid(String pid);

    /**
     * 通过pid 获取sort值
     * @param pid
     * @return
     */
    Integer selectSortByPid(String pid);

    /**
     * 查询最大sort 值
     * @param status
     * @return
     */
    Integer maxActivitySort (Integer status);

    /**
     * 判断pid是不是有效
     * @param pid
     * @param status
     * @return
     */
    Integer countByPidSelective (@Param("pid") String pid, @Param("status") Integer status);

    /**
     * 判断name是不是重复
     * @param name
     * @param status
     * @return
     */
    Integer countByName (@Param("name") String name, @Param("status") Integer status);

    /**
     * 判断name是不是和其他活动名称重复
     * @param map
     * @return
     */
    Integer countByNameAndPidSelective (Map<String, Object> map);

    /**
    * 新增(非空判断)
    * @param activity
    */
    int insertSelective(Activity activity);

    /**
    * 通过pid修改（非空判断）
    * @param activity
    */
    int updateByPidSelective(Activity activity);

    /**
    * 通过pid逻辑删除
    * @param pids
    */
    int deleteByPidSelective(List<String> pids);
}
