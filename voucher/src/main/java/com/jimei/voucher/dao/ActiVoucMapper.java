package com.jimei.voucher.dao;

import java.util.List;

import com.jimei.voucher.pojo.ActiVouc;
import com.jimei.voucher.vo.ActiVoucVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public interface ActiVoucMapper {

    /**
    * 列表数据
    * @param params
    */
    List<ActiVoucVO> selectListByActivity(String activityPid);

    /**
    * 通过pid 获取数据
    * @param pid
    */
    ActiVouc selectByPid(String pid);

    /**
    * 新增(非空判断)
    * @param actiVouc
    */
    int insertSelective(ActiVouc actiVouc);

    /**
    * 通过pid修改（非空判断）
    * @param actiVouc
    */
    int updateByPidSelective(ActiVouc actiVouc);

    /**
    * 通过pid逻辑删除
    * @param pids
    */
    int deleteByPidSelective(List<String> pids);
}
