package com.jimei.voucher.dao;

import java.util.List;
import java.util.Map;

import com.jimei.voucher.pojo.Payment;
import com.jimei.voucher.vo.PaymentVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public interface PaymentMapper {

    /**
    * 列表数据
    * @param params
    */
    List<PaymentVO> selectListByParams(Map<String, Object> params);

    /**
    * 新增(非空判断)
    * @param payment
    */
    int insertSelective(Payment payment);

    /**
    * 通过pid修改（非空判断）
    * @param payment
    */
    int updateByPidSelective(Payment payment);

    /**
     * 根据支付回调修改支付状态
     * @param payment
     * @return
     */
    int updatePayStatusByPid(Payment payment);

}
