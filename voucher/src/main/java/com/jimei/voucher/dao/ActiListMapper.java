package com.jimei.voucher.dao;

import java.util.Map;
import java.util.List;

import com.jimei.voucher.pojo.ActiList;
import com.jimei.voucher.vo.ActiListVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public interface ActiListMapper {

    /**
    * 列表数据
    * @param params
    */
    List<ActiListVO> selectListByParams(Map<String, Object> params);

    /**
    * 通过pid 获取数据
    * @param pid
    */
    ActiList selectByPid(String pid);

    List<ActiListVO> selectActivityList(String activityPid);
    
    /**
    * 新增(非空判断)
    * @param actiList
    */
    int insertSelective(ActiList actiList);

    /**
    * 通过pid修改（非空判断）
    * @param actiList
    */
    int updateByPidSelective(ActiList actiList);

    /**
    * 通过pid逻辑删除
    * @param pids
    */
    int deleteByPidSelective(List<String> pids);

}
