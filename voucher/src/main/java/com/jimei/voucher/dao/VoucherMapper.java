package com.jimei.voucher.dao;

import java.util.Map;
import java.util.List;

import com.jimei.voucher.pojo.Voucher;
import com.jimei.voucher.vo.VoucherVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public interface VoucherMapper {

    /**
    * 列表数据
    * @param params
    */
    List<Voucher> selectListByParams(Map<String, Object> params);

    /**
    * 通过pid 获取数据
    * @param pid
    */
    Voucher selectByPid(String pid);

    /**
    * 新增(非空判断)
    * @param voucher
    */
    int insertSelective(Voucher voucher);

    /**
    * 通过pid修改（非空判断）
    * @param voucher
    */
    int updateByPidSelective(Voucher voucher);

    /**
    * 通过pid逻辑删除
    * @param pids
    */
    int deleteByPidSelective(List<String> pids);
}
