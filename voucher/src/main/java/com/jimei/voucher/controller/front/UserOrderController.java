package com.jimei.voucher.controller.front;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.service.OrderService;
import com.jimei.voucher.vo.OrderVO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * 
 * 2018年9月14日
 */
@RequestMapping("/order")
@RestController("UserOrderController")
public class UserOrderController {
    
    @Autowired
    private OrderService orderService;
    
    /**
     * 用户订单列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页数据", httpMethod = "GET")
    public ServerResponse<PageBean<OrderVO>> list(@RequestParam Map<String, Object> params){
        return orderService.selectListByParams(params);
    }


    /**
     * 详情
     */
    @GetMapping("/info/{pid}")
    @ApiOperation(value = "详情数据", httpMethod = "GET")
    public ServerResponse<OrderVO> info(@ApiParam(name = "pid", value = "pid参数") @PathVariable("pid") String pid){
        if(StringUtils.isEmpty(pid)){
            return ServerResponse.createByErrorMessage("查询详情所需参数不能为空");
        }
        return orderService.selectByPid(pid);
    }
    
    /**
     * 下单
     * @param orderVo
     */
    @PostMapping("/save")
    @ApiOperation(value = "下单", httpMethod = "POST")
    public ServerResponse<String> save(@RequestBody OrderVO orderVo){
        return orderService.add(orderVo);
    }
    
    /**
     * 删除订单
     * @param pids
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除订单", httpMethod = "DELETE")
    public ServerResponse<String> delete(@ApiParam(name = "pids", value = "pid参数的数组") @RequestBody String[] pids){
        if(pids == null || pids.length <= 0){
            return ServerResponse.createByErrorMessage("删除所需参数不能为空");
        }
        return orderService.deleteByPids(Arrays.asList(pids));
    }

}
