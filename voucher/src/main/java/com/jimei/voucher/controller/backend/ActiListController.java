package com.jimei.voucher.controller.backend;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.jimei.common.ServerResponse;
import com.jimei.voucher.service.ActiListService;
import com.jimei.voucher.vo.ActiListVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:44:18
 */
@RestController
@RequestMapping("/backend/actilist")
@Api(value = "ActiListController", description = "ActiList业务接口")
public class ActiListController {
    private static Logger logger = LoggerFactory.getLogger(ActiListController.class);

    @Autowired
    private ActiListService actiListService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页数据", httpMethod = "GET")
    public ServerResponse<List<ActiListVO>> list(String activityPid){
        logger.info("list {}", activityPid);
        return actiListService.selectActiListResp(activityPid);
    }


    /**
     * 详情
     */
    @GetMapping("/info/{pid}")
    @ApiOperation(value = "详情数据", httpMethod = "GET")
    public ServerResponse<ActiListVO> info(@ApiParam(name = "pid", value = "pid参数") @PathVariable("pid") String pid){
        logger.info("info {}", pid);
        if(StringUtils.isEmpty(pid)){
            return ServerResponse.createByErrorMessage("查询详情所需参数不能为空");
        }
        return actiListService.selectByPid(pid);
    }

    /**
     * 保存
     * @param actiListVo
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增数据", httpMethod = "POST")
    public ServerResponse<String> save(@RequestBody ActiListVO actiListVo){
        logger.info("save {}", actiListVo);
        return actiListService.add(actiListVo);
    }

    /**
     * 修改
     * @param actiListVo
     */
    @PutMapping("/update")
    @ApiOperation(value = "修改数据", httpMethod = "PUT")
    public ServerResponse<String> update(@RequestBody ActiListVO actiListVo){
        logger.info("update {}", actiListVo);
        if (actiListVo == null) {
            return ServerResponse.createByErrorMessage("修改所需参数不能为空");
        }
        if(StringUtils.isEmpty(actiListVo.getPid())){
            return ServerResponse.createByErrorMessage("修改所需pid参数不能为空");
        }

        return actiListService.update(actiListVo);

    }

    /**
     * 逻辑删除（批量）
     * @param pids
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除数据", httpMethod = "DELETE")
    public ServerResponse<String> delete(@ApiParam(name = "pids", value = "pid参数的json数组") @RequestBody String[] pids){
        logger.info("delete {}", JSON.toJSONString(pids));
        if(pids == null || pids.length <= 0){
            return ServerResponse.createByErrorMessage("删除所需参数不能为空");
        }
        return actiListService.deleteByPids(Arrays.asList(pids));
    }

}
