package com.jimei.voucher.controller.backend;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.service.PaymentService;
import com.jimei.voucher.vo.PaymentVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:44:18
 */
@RestController
@RequestMapping("/backend/payment")
@Api(value = "PaymentController", description = "Payment业务接口")
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页数据", httpMethod = "GET")
    public ServerResponse<PageBean<PaymentVO>> list(@RequestParam Map<String, Object> params){
        return paymentService.selectListByParams(params);
    }
}
