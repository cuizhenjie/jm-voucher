package com.jimei.voucher.controller.backend;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/backend/dashboard")
public class DashboardController {
    
    @RequestMapping("")
    public String index() {
        return "";
    }
}
