package com.jimei.voucher.controller.backend;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.service.OrderService;
import com.jimei.voucher.vo.OrderVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:44:18
 */
@RestController
@RequestMapping("/backend/order")
@Api(value = "OrderController", description = "Order业务接口")
public class OrderController {

    @Resource
    private OrderService orderService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页数据", httpMethod = "GET")
    public ServerResponse<PageBean<OrderVO>> list(@RequestParam Map<String, Object> params){
        return orderService.selectListByParams(params);
    }


    /**
     * 详情
     */
    @GetMapping("/info/{pid}")
    @ApiOperation(value = "详情数据", httpMethod = "GET")
    public ServerResponse<OrderVO> info(@ApiParam(name = "pid", value = "pid参数") @PathVariable("pid") String pid){
        if(StringUtils.isEmpty(pid)){
            return ServerResponse.createByErrorMessage("查询详情所需参数不能为空");
        }
        return orderService.selectByPid(pid);
    }

}
