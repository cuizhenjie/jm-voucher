package com.jimei.voucher.controller.front;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.service.ActivityService;
import com.jimei.voucher.vo.ActivityVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户抢券中心
 * 
 * 2018年9月18日
 */
@RequestMapping("/activity")
@RestController
@Api(value = "UserActivityController", description = "用户抢券中心")
public class UserActivityController {
//    private static Logger logger = LoggerFactory.getLogger(ActivityController.class);
    
    @Autowired
    private ActivityService activityService;

    /**
     * 列表
     * @param name 活动名称
     * @param shop 活动门店
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "活动分页列表", httpMethod = "GET")
    public ServerResponse<PageBean<ActivityVO>> list(String name, String shop){
        Map<String, Object> params = new HashMap<>();
        params.put("status", 1);
        if(StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if(StringUtils.isNotBlank(shop)) {
            params.put("shop", shop);
        }
        return activityService.selectListByParams(params);
    }
    
    /**
     * 活动场次
     * @param id 活动Pid
     * @return
     */
    @GetMapping("/{id}")
    public ServerResponse<ActivityVO> detail(String id){
        return activityService.selectDetail(id);
    }
    
}
