package com.jimei.voucher.controller.backend;

import java.util.Arrays;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jimei.common.ServerResponse;
import com.jimei.voucher.service.ActivityService;
import com.jimei.voucher.vo.ActivityVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:44:18
 */
@RestController
@RequestMapping("/backend/activity")
@Api(value = "ActivityController", description = "Activity业务接口")
public class ActivityController {

    @Resource
    private ActivityService activityService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页数据", httpMethod = "GET")
    public ServerResponse list(@RequestParam Map<String, Object> params){
        return activityService.selectListByParams(params);
    }


    /**
     * 详情
     */
    @GetMapping("/info/{pid}")
    @ApiOperation(value = "详情数据", httpMethod = "GET")
    public ServerResponse info(@ApiParam(name = "pid", value = "pid参数") @PathVariable("pid") String pid){

        if(StringUtils.isEmpty(pid)){
            return ServerResponse.createByErrorMessage("查询详情所需参数不能为空");
        }

        return activityService.selectByPid(pid);
    }

    /**
     * 保存
     * @param activityVo
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增数据", httpMethod = "POST")
    public ServerResponse save(@RequestBody ActivityVO activityVo){
        return activityService.add(activityVo);
    }

    /**
     * 修改
     * @param activityVo
     */
    @PutMapping("/update")
    @ApiOperation(value = "修改数据", httpMethod = "PUT")
    public ServerResponse update(@RequestBody ActivityVO activityVo){

        if (activityVo == null) {
            return ServerResponse.createByErrorMessage("修改所需参数不能为空");
        }
        if(StringUtils.isEmpty(activityVo.getPid())){
            return ServerResponse.createByErrorMessage("修改所需pid参数不能为空");
        }

        return activityService.update(activityVo);

    }

    /**
     * 置顶活动
     * @param pid
     * @return
     */
    @RequestMapping (value = "/top/{pid}" ,method = RequestMethod.POST)
    @ResponseBody
    ServerResponse updateActivityTop (@PathVariable ("pid") String pid) {

        if (pid == null) {
            return ServerResponse.createByErrorMessage("请求参数出错！！！pid不能为空");
        }
        return activityService.updateActivityTop(pid);
    }

    /**
     * 逻辑删除（批量）
     * @param pids
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除数据", httpMethod = "DELETE")
    public ServerResponse delete(@ApiParam(name = "pids", value = "pid参数的数组") @RequestBody String[] pids){

        if(pids == null || pids.length <= 0){
            return ServerResponse.createByErrorMessage("删除所需参数不能为空");
        }

        return activityService.deleteByPids(Arrays.asList(pids));
    }

}
