package com.jimei.voucher.exception;

import com.alibaba.fastjson.JSON;
import com.jimei.common.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 异常处理器
 * 
 * @author czj
 */
@Component
public class VoucherExceptionHandler implements HandlerExceptionResolver {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		ServerResponse serverResponse = null;
		try {
			response.setContentType("application/json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			
			if (ex instanceof VoucherException) {
				serverResponse = ServerResponse.createByErrorCodeMessage( ((VoucherException) ex).getCode(),ex.getMessage());

			}else if(ex instanceof DuplicateKeyException){
				serverResponse = ServerResponse.createByErrorMessage("数据库中已存在该记录");
			}else{
				serverResponse = ServerResponse.createByErrorMessage("未知异常");
			}
			
			//记录异常日志
			logger.error(ex.getMessage(), ex);
			
			String json = JSON.toJSONString(serverResponse);
			response.getWriter().print(json);
		} catch (Exception e) {
			logger.error("ExceptionHandler 异常处理失败", e);
		}
		return new ModelAndView();
	}
}
