package com.jimei.voucher.vo;



import com.jimei.common.modules.BaseVO;

import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class PaymentVO extends BaseVO {

	private static final long serialVersionUID = 1L;

    /**
     * 订单PID，订单号
     */
    private String orderPid;

    /**
     * 支付方式，0微信；1支付宝
     */
    private Integer payType;

    /**
     * 支付金额
     */
    private Integer payAmount;

    /**
     * 支付状态,0未支付；-1已取消；1已支付
     */
    private Integer payStatus;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 支付流水
     */
    private String dealCode;




    public String getOrderPid() {
        return orderPid;
    }

    public void setOrderPid(String orderPid) {
        this.orderPid = orderPid;
    }


    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }


    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }


    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }


    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }


    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }


}
