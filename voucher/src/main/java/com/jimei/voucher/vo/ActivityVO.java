package com.jimei.voucher.vo;



import com.jimei.common.modules.BaseVO;

import java.util.Date;
import java.util.List;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public class ActivityVO extends BaseVO {

	private static final long serialVersionUID = 1L;

    /**
     *活动名称
     */
    private String name;

    /**
     *图片,取banner第一张
     */
    private String img;

    /**
     *banner,','分隔
     */
    private String banner;

    /**
     *参与门店
     */
    private String shop;

    /**
     *价格(分)
     */
    private Integer amount;

    /**
     *活动类型 1正常2不需要调用erp3不需要支付
     */
    private Integer type;

    /**
     *券类型,1代金券;2储值卡;3资格券
     */
    private Integer voucherMode;

    /**
     *总派发套数
     */
    private Integer count;

    /**
     *是否可重复购买0不可1可
     */
    private Integer rebuy;

    /**
     *活动开始时间
     */
    private Date startTime;

    /**
     *活动结束时间
     */
    private Date endTime;


    /**
     *操作人，用户PID
     */
    private String operator;

    /**
     *排序,默认0
     */
    private Integer sort;


    /**
     *活动介绍
     */
    private String intro;

    /**
     * 活动场次
     */
    private List<ActiListVO> actiListVOs;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }


    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }


    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Integer getRebuy() {
        return rebuy;
    }

    public void setRebuy(Integer rebuy) {
        this.rebuy = rebuy;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getVoucherMode() {
        return voucherMode;
    }

    public void setVoucherMode(Integer voucherMode) {
        this.voucherMode = voucherMode;
    }

    public List<ActiListVO> getActiListVOs() {
        return actiListVOs;
    }

    public void setActiListVOs(List<ActiListVO> actiListVOs) {
        this.actiListVOs = actiListVOs;
    }

    @Override
    public String toString() {
        return "ActivityVO [name=" + name + ", img=" + img + ", banner=" + banner + ", shop=" + shop + ", amount="
                + amount + ", type=" + type + ", voucherMode=" + voucherMode + ", count=" + count + ", rebuy=" + rebuy
                + ", startTime=" + startTime + ", endTime=" + endTime + ", operator=" + operator + ", sort=" + sort
                + ", intro=" + intro + ", actiListVOs=" + actiListVOs + "]";
    }
    
}
