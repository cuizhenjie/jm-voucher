package com.jimei.voucher.vo;



import com.jimei.common.modules.BaseVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class VoucherVO extends BaseVO {

	private static final long serialVersionUID = 1L;

    /**
     * 券码
     */
    private Long code;

    /**
     * 券秘钥
     */
    private String pass;

    /**
     *  活动PID
     */
    private String activityPid;

    /**
     * 活动场次pid
     */
    private String actiListPid;

    /**
     * 活动关联券pid
     */
    private String actiVoucPid;

    /**
     * 券类型1代金券;2储值卡;3资格券
     */
    private Integer voucherMode;

    /**
     * 券种
     */
    private String voucherType;

    /**
     * 券状态,0未派发；1已派发,2核销
     */
    private Integer voucherStatus;

    /**
     * 订单PID,订单号
     */
    private String orderPid;




    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }


    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getActiListPid() {
        return actiListPid;
    }

    public void setActiListPid(String actiListPid) {
        this.actiListPid = actiListPid;
    }


    public String getActiVoucPid() {
        return actiVoucPid;
    }

    public void setActiVoucPid(String actiVoucPid) {
        this.actiVoucPid = actiVoucPid;
    }


    public Integer getVoucherMode() {
        return voucherMode;
    }

    public void setVoucherMode(Integer voucherMode) {
        this.voucherMode = voucherMode;
    }


    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }


    public Integer getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(Integer voucherStatus) {
        this.voucherStatus = voucherStatus;
    }


    public String getOrderPid() {
        return orderPid;
    }

    public void setOrderPid(String orderPid) {
        this.orderPid = orderPid;
    }


}
