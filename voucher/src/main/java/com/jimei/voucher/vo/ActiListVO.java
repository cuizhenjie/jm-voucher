package com.jimei.voucher.vo;



import java.util.Date;

import com.jimei.common.modules.BaseVO;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class ActiListVO extends BaseVO {

	private static final long serialVersionUID = 1L;

    /**
     * 活动PID
     */
    private String activityPid;

    /**
     * 场次名称
     */
    private String name;

    /**
     * 单场派发套数
     */
    private Integer count;

    /**
     * 场次开始时间
     */
    private Date startTime;

    /**
     * 场次结束时间
     */
    private Date endTime;

    /**
     * 操作人，用户PID
     */
    private String operator;


    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return "ActiListVO [activityPid=" + activityPid + ", name=" + name + ", count=" + count + ", startTime="
                + startTime + ", endTime=" + endTime + ", operator=" + operator + ", id=" + id + ", pid=" + pid
                + ", status=" + status + ", createTime=" + createTime + ", updateTime=" + updateTime + ", version="
                + version + "]";
    }

}
