package com.jimei.voucher.vo;



import com.jimei.common.modules.BaseVO;

import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class OrderVO extends BaseVO {

	private static final long serialVersionUID = 1L;

    /**
     * 活动PID
     */
    private String activityPid;

    /**
     * 活动场次PID
     */
    private String actiListPid;

    /**
     * 用户PID
     */
    private String userPid;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 支付方式，0微信；1支付宝
     */
    private Integer payType;

    /**
     * 应付金额
     */
    private Integer payAmount;

    /**
     * 优惠金额
     */
    private Integer discountAmount;

    /**
     * 订单总额
     */
    private Integer totalAmount;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 支付流水号
     */
    private String dealCode;

    /**
     * 订单状态,0未支付；-1已取消；1已支付；8已完成
     */
    private Integer orderStatus;




    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getActiListPid() {
        return actiListPid;
    }

    public void setActiListPid(String actiListPid) {
        this.actiListPid = actiListPid;
    }


    public String getUserPid() {
        return userPid;
    }

    public void setUserPid(String userPid) {
        this.userPid = userPid;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }


    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }


    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }


    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }


    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }


    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }


    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }


}
