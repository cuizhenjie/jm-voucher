package com.jimei.voucher.vo;



import com.jimei.common.modules.BaseVO;

import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class ActiVoucVO extends BaseVO {

	private static final long serialVersionUID = 1L;

    /**
     * 活动PID
     */
    private String activityPid;

    /**
     * 券种
     */
    private String voucherType;

    /**
     * 券名称
     */
    private String name;

    /**
     * 使用品类,0全部,1家具,2建材
     */
    private Integer category;

    /**
     * 券有效期开始时间
     */
    private Date startTime;

    /**
     * 券有效期结束时间
     */
    private Date endTime;

    /**
     * 单次派发数量
     */
    private Integer singleNum;

    /**
     * 操作人，用户PID
     */
    private String operator;

    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getSingleNum() {
        return singleNum;
    }

    public void setSingleNum(Integer singleNum) {
        this.singleNum = singleNum;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return "ActiVoucVO [activityPid=" + activityPid + ", voucherType=" + voucherType + ", name=" + name
                + ", category=" + category + ", startTime=" + startTime + ", endTime=" + endTime + ", singleNum="
                + singleNum + ", operator=" + operator + ", id=" + id + ", pid=" + pid + ", status=" + status
                + ", createTime=" + createTime + ", updateTime=" + updateTime + ", version=" + version + "]";
    }


}
