package com.jimei.voucher.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * swagger 配置
 * @author czj
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/swagger/**").addResourceLocations("classpath:/static/swagger/");
    }


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //加了ApiOperation注解的类，才生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //包下的类，才生成接口文档
                //.apis(RequestHandlerSelectors.basePackage("com.jimei.controller"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(security());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("集美")
                .description("集美券业务API文档")
                .termsOfServiceUrl("https://www.jimei.cn")
                .version("1.0")
                .build();
    }

    private List<ApiKey> security() {
        return newArrayList(
                new ApiKey("jimei-develop", "jimei", "eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiNTc3YzAwYmEtMmJiZC00NWE0LWE3MGQtOGU2OGQyOTUwM2NlIiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiI0ZTkzZDZmYy01OTE0LTQzMGEtYWIwNy1hYjMxN2ZkYTA0YjMiLCJpYXQiOjE1MzYyMzY2Njl9.iHEKktBcIcoHKJuh3x-r6IvfmrQdQrBluf6RINpdEzg9v69PE8t8KZyUotNw0NsAzdPdyJcHF1xdsuttxI7tBQ")
        );
    }

}