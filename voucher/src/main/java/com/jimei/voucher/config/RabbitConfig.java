package com.jimei.voucher.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbit队列配置
 */
@Configuration
public class RabbitConfig {
    
    /**
     * 订单交换
     */
    public static final String ORDER_EXCHANGE = "order.exchange";

    /**
     * 取消订单交换
     */
    public static final String ORDER_CANCEL_EXCHANGE = "order.cancel.exchange";
    
    public static final String ORDER_QUEUE = "order.queue";

    public static final String ORDER_CANCEL_QUEUE = "order.cancel.queue";
    
    public static final String DIRECT_ORDER = "direct.order";
    
    @Bean
    public Exchange orderExchange() {
        return ExchangeBuilder.directExchange(ORDER_EXCHANGE).build();
    }
    
    @Bean
    public Exchange orderCancelExchange() {
        return ExchangeBuilder.directExchange(ORDER_CANCEL_EXCHANGE).build();
    }
    
    @Bean
    public Queue orderQueue() {
        return QueueBuilder
                .durable(ORDER_QUEUE)
                .withArgument("x-dead-letter-exchange", ORDER_CANCEL_EXCHANGE)
                .build();
    }
    
    @Bean
    public Queue orderCancelQueue() {
        return QueueBuilder.durable(ORDER_CANCEL_QUEUE).build();
    }
    
    @Bean
    public Binding orderBinding(@Qualifier("orderExchange")Exchange exchange, @Qualifier("orderQueue")Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(DIRECT_ORDER).noargs();
    }
    
    @Bean
    public Binding orderCancelBinding(@Qualifier("orderCancelExchange")Exchange exchange, @Qualifier("orderCancelQueue")Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(DIRECT_ORDER).noargs();
    }
}
