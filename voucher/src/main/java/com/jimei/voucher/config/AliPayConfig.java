package com.jimei.voucher.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayConstants;
import com.alipay.api.DefaultAlipayClient;

/**
 * 支付宝接口
 * 
 * 2018年9月14日
 */
@Configuration
public class AliPayConfig {
    
    /**
     * 支付宝网关（固定）
     * https://openapi.alipay.com/gateway.do
     */
    private String serverUrl = "https://openapi.alipay.com/gateway.do";
    
    /**
     * APPID 即创建应用后生成
     */
    @Value("${alipay.appId}")
    private String appId;
    
    /**
     * 开发者私钥，由开发者自己生成
     */
    @Value("${alipay.privateKey}")
    private String privateKey;
    
    /**
     * 参数返回格式，只支持json
     */
    private String format = AlipayConstants.FORMAT_JSON;
    
    /**
     * 编码集，支持GBK/UTF-8
     */
    private String charset = AlipayConstants.CHARSET_UTF8;
    
    /**
     * 支付宝公钥，由支付宝生成
     */
    @Value("${alipay.alipayPublicKey}")
    private String alipayPublicKey;
    
    /**
     * 商户生成签名字符串所使用的签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
     */
    private String signType = AlipayConstants.SIGN_TYPE_RSA2;

    @Bean
    public AlipayClient alipayClient() {
        return new DefaultAlipayClient(serverUrl, appId, privateKey, format, charset, alipayPublicKey, signType);
    }
    
}
