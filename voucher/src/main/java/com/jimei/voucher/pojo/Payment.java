package com.jimei.voucher.pojo;


import com.google.common.base.Objects;
import com.jimei.common.modules.BaseModel;
import com.jimei.voucher.vo.PaymentVO;


import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class Payment extends BaseModel<PaymentVO> {

	private static final long serialVersionUID = 1L;


    /**
     * 订单PID，订单号
     */
    private String orderPid;

    /**
     * 支付方式，0微信；1支付宝
     */
    private Integer payType;

    /**
     * 支付金额
     */
    private Integer payAmount;

    /**
     * 支付状态,0未支付；-1已取消；1已支付
     */
    private Integer payStatus;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 支付流水
     */
    private String dealCode;


    /**
     *  全部参数构造
     */
    public Payment(Long id, String pid, String orderPid, Integer payType, Integer payAmount, Integer payStatus, Date payTime, String dealCode, Date createTime, Date updateTime, Integer status, Integer version){
        this.id = id;
        this.pid = pid;
        this.orderPid = orderPid;
        this.payType = payType;
        this.payAmount = payAmount;
        this.payStatus = payStatus;
        this.payTime = payTime;
        this.dealCode = dealCode;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
        this.version = version;
    }
    /**
     *  默认构造
     */
    public Payment() {}



    public String getOrderPid() {
        return orderPid;
    }

    public void setOrderPid(String orderPid) {
        this.orderPid = orderPid;
    }


    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }


    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }


    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }


    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }


    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }



    @Override
    public String toString() {
        return "Payment{" +
           "id=" + id +
              ", pid=" + pid +
              ", orderPid=" + orderPid +
              ", payType=" + payType +
              ", payAmount=" + payAmount +
              ", payStatus=" + payStatus +
              ", payTime=" + payTime +
              ", dealCode=" + dealCode +
              ", createTime=" + createTime +
              ", updateTime=" + updateTime +
              ", status=" + status +
              ", version=" + version +
       '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Payment model = (Payment) o;

        return
            Objects.equal(id, model.id)
                && Objects.equal(pid, model.pid)
                && Objects.equal(orderPid, model.orderPid)
                && Objects.equal(payType, model.payType)
                && Objects.equal(payAmount, model.payAmount)
                && Objects.equal(payStatus, model.payStatus)
                && Objects.equal(payTime, model.payTime)
                && Objects.equal(dealCode, model.dealCode)
                && Objects.equal(createTime, model.createTime)
                && Objects.equal(updateTime, model.updateTime)
                && Objects.equal(status, model.status)
                && Objects.equal(version, model.version)
    ;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pid, orderPid, payType, payAmount, payStatus, payTime, dealCode, createTime, updateTime, status, version);
    }


}
