package com.jimei.voucher.pojo;


import com.google.common.base.Objects;
import com.jimei.common.modules.BaseModel;
import com.jimei.voucher.vo.VoucherVO;


import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class Voucher extends BaseModel<VoucherVO> {

	private static final long serialVersionUID = 1L;


    /**
     * 券码
     */
    private Long code;

    /**
     * 券秘钥
     */
    private String pass;

    /**
     *  活动PID
     */
    private String activityPid;

    /**
     * 活动场次pid
     */
    private String actiListPid;

    /**
     * 活动关联券pid
     */
    private String actiVoucPid;

    /**
     * 券类型1代金券;2储值卡;3资格券
     */
    private Integer voucherMode;

    /**
     * 券种
     */
    private String voucherType;

    /**
     * 券状态,0未派发；1已派发,2核销
     */
    private Integer voucherStatus;

    /**
     * 订单PID,订单号
     */
    private String orderPid;


    /**
     *  全部参数构造
     */
    public Voucher(Long id, String pid, Long code, String pass, String activityPid, String actiListPid, String actiVoucPid, Integer voucherMode, String voucherType, Integer voucherStatus, String orderPid, Date createTime, Date updateTime, Integer status, Integer version){
        this.id = id;
        this.pid = pid;
        this.code = code;
        this.pass = pass;
        this.activityPid = activityPid;
        this.actiListPid = actiListPid;
        this.actiVoucPid = actiVoucPid;
        this.voucherMode = voucherMode;
        this.voucherType = voucherType;
        this.voucherStatus = voucherStatus;
        this.orderPid = orderPid;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
        this.version = version;
    }
    /**
     *  默认构造
     */
    public Voucher() {}



    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }


    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getActiListPid() {
        return actiListPid;
    }

    public void setActiListPid(String actiListPid) {
        this.actiListPid = actiListPid;
    }


    public String getActiVoucPid() {
        return actiVoucPid;
    }

    public void setActiVoucPid(String actiVoucPid) {
        this.actiVoucPid = actiVoucPid;
    }


    public Integer getVoucherMode() {
        return voucherMode;
    }

    public void setVoucherMode(Integer voucherMode) {
        this.voucherMode = voucherMode;
    }


    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }


    public Integer getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(Integer voucherStatus) {
        this.voucherStatus = voucherStatus;
    }


    public String getOrderPid() {
        return orderPid;
    }

    public void setOrderPid(String orderPid) {
        this.orderPid = orderPid;
    }



    @Override
    public String toString() {
        return "Voucher{" +
           "id=" + id +
              ", pid=" + pid +
              ", code=" + code +
              ", pass=" + pass +
              ", activityPid=" + activityPid +
              ", actiListPid=" + actiListPid +
              ", actiVoucPid=" + actiVoucPid +
              ", voucherMode=" + voucherMode +
              ", voucherType=" + voucherType +
              ", voucherStatus=" + voucherStatus +
              ", orderPid=" + orderPid +
              ", createTime=" + createTime +
              ", updateTime=" + updateTime +
              ", status=" + status +
              ", version=" + version +
       '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Voucher model = (Voucher) o;

        return
            Objects.equal(id, model.id)
                && Objects.equal(pid, model.pid)
                && Objects.equal(code, model.code)
                && Objects.equal(pass, model.pass)
                && Objects.equal(activityPid, model.activityPid)
                && Objects.equal(actiListPid, model.actiListPid)
                && Objects.equal(actiVoucPid, model.actiVoucPid)
                && Objects.equal(voucherMode, model.voucherMode)
                && Objects.equal(voucherType, model.voucherType)
                && Objects.equal(voucherStatus, model.voucherStatus)
                && Objects.equal(orderPid, model.orderPid)
                && Objects.equal(createTime, model.createTime)
                && Objects.equal(updateTime, model.updateTime)
                && Objects.equal(status, model.status)
                && Objects.equal(version, model.version)
    ;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pid, code, pass, activityPid, actiListPid, actiVoucPid, voucherMode, voucherType, voucherStatus, orderPid, createTime, updateTime, status, version);
    }


}
