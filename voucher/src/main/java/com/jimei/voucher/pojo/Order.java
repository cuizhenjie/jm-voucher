package com.jimei.voucher.pojo;


import com.google.common.base.Objects;
import com.jimei.common.modules.BaseModel;
import com.jimei.voucher.vo.OrderVO;


import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class Order extends BaseModel<OrderVO> {

	private static final long serialVersionUID = 1L;


    /**
     * 活动PID
     */
    private String activityPid;

    /**
     * 活动场次PID
     */
    private String actiListPid;

    /**
     * 用户PID
     */
    private String userPid;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 支付方式，0微信；1支付宝
     */
    private Integer payType;

    /**
     * 应付金额
     */
    private Integer payAmount;

    /**
     * 优惠金额
     */
    private Integer discountAmount;

    /**
     * 订单总额
     */
    private Integer totalAmount;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 支付流水号
     */
    private String dealCode;

    /**
     * 订单状态,0未支付；-1已取消；1已支付；8已完成
     */
    private Integer orderStatus;


    /**
     *  全部参数构造
     */
    public Order(Long id, String pid, String activityPid, String actiListPid, String userPid, String phone, Integer payType, Integer payAmount, Integer discountAmount, Integer totalAmount, Date payTime, String dealCode, Integer orderStatus, Date createTime, Date updateTime, Integer status, Integer version){
        this.id = id;
        this.pid = pid;
        this.activityPid = activityPid;
        this.actiListPid = actiListPid;
        this.userPid = userPid;
        this.phone = phone;
        this.payType = payType;
        this.payAmount = payAmount;
        this.discountAmount = discountAmount;
        this.totalAmount = totalAmount;
        this.payTime = payTime;
        this.dealCode = dealCode;
        this.orderStatus = orderStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
        this.version = version;
    }
    /**
     *  默认构造
     */
    public Order() {}



    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getActiListPid() {
        return actiListPid;
    }

    public void setActiListPid(String actiListPid) {
        this.actiListPid = actiListPid;
    }


    public String getUserPid() {
        return userPid;
    }

    public void setUserPid(String userPid) {
        this.userPid = userPid;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }


    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }


    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }


    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }


    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }


    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }


    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }



    @Override
    public String toString() {
        return "Order{" +
           "id=" + id +
              ", pid=" + pid +
              ", activityPid=" + activityPid +
              ", actiListPid=" + actiListPid +
              ", userPid=" + userPid +
              ", phone=" + phone +
              ", payType=" + payType +
              ", payAmount=" + payAmount +
              ", discountAmount=" + discountAmount +
              ", totalAmount=" + totalAmount +
              ", payTime=" + payTime +
              ", dealCode=" + dealCode +
              ", orderStatus=" + orderStatus +
              ", createTime=" + createTime +
              ", updateTime=" + updateTime +
              ", status=" + status +
              ", version=" + version +
       '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Order model = (Order) o;

        return
            Objects.equal(id, model.id)
                && Objects.equal(pid, model.pid)
                && Objects.equal(activityPid, model.activityPid)
                && Objects.equal(actiListPid, model.actiListPid)
                && Objects.equal(userPid, model.userPid)
                && Objects.equal(phone, model.phone)
                && Objects.equal(payType, model.payType)
                && Objects.equal(payAmount, model.payAmount)
                && Objects.equal(discountAmount, model.discountAmount)
                && Objects.equal(totalAmount, model.totalAmount)
                && Objects.equal(payTime, model.payTime)
                && Objects.equal(dealCode, model.dealCode)
                && Objects.equal(orderStatus, model.orderStatus)
                && Objects.equal(createTime, model.createTime)
                && Objects.equal(updateTime, model.updateTime)
                && Objects.equal(status, model.status)
                && Objects.equal(version, model.version)
    ;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pid, activityPid, actiListPid, userPid, phone, payType, payAmount, discountAmount, totalAmount, payTime, dealCode, orderStatus, createTime, updateTime, status, version);
    }


}
