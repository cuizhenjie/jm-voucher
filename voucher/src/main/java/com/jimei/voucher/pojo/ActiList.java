package com.jimei.voucher.pojo;


import com.google.common.base.Objects;
import com.jimei.common.modules.BaseModel;
import com.jimei.voucher.vo.ActiListVO;


import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class ActiList extends BaseModel<ActiListVO> {

	private static final long serialVersionUID = 1L;


    /**
     * 活动PID
     */
    private String activityPid;

    /**
     * 场次名称
     */
    private String name;

    /**
     * 单场派发套数
     */
    private Integer count;

    /**
     * 场次开始时间
     */
    private Date startTime;

    /**
     * 场次结束时间
     */
    private Date endTime;

    /**
     * 操作人，用户PID
     */
    private String operator;


    /**
     *  全部参数构造
     */
    public ActiList(Long id, String pid, String activityPid, String name, Integer count, Date startTime, Date endTime, Date createTime, Date updateTime, String operator, Integer status, Integer version){
        this.id = id;
        this.pid = pid;
        this.activityPid = activityPid;
        this.name = name;
        this.count = count;
        this.startTime = startTime;
        this.endTime = endTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.operator = operator;
        this.status = status;
        this.version = version;
    }
    /**
     *  默认构造
     */
    public ActiList() {}



    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }



    @Override
    public String toString() {
        return "ActiList{" +
           "id=" + id +
              ", pid=" + pid +
              ", activityPid=" + activityPid +
              ", name=" + name +
              ", count=" + count +
              ", startTime=" + startTime +
              ", endTime=" + endTime +
              ", createTime=" + createTime +
              ", updateTime=" + updateTime +
              ", operator=" + operator +
              ", status=" + status +
              ", version=" + version +
       '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        ActiList model = (ActiList) o;

        return
            Objects.equal(id, model.id)
                && Objects.equal(pid, model.pid)
                && Objects.equal(activityPid, model.activityPid)
                && Objects.equal(name, model.name)
                && Objects.equal(count, model.count)
                && Objects.equal(startTime, model.startTime)
                && Objects.equal(endTime, model.endTime)
                && Objects.equal(createTime, model.createTime)
                && Objects.equal(updateTime, model.updateTime)
                && Objects.equal(operator, model.operator)
                && Objects.equal(status, model.status)
                && Objects.equal(version, model.version)
    ;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pid, activityPid, name, count, startTime, endTime, createTime, updateTime, operator, status, version);
    }


}
