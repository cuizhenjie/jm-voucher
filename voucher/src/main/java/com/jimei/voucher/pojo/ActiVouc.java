package com.jimei.voucher.pojo;


import com.google.common.base.Objects;
import com.jimei.common.modules.BaseModel;
import com.jimei.voucher.vo.ActiVoucVO;


import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-10 15:10:43
 */
public class ActiVouc extends BaseModel<ActiVoucVO> {

	private static final long serialVersionUID = 1L;


    /**
     * 活动PID
     */
    private String activityPid;

    /**
     * 券种
     */
    private String voucherType;

    /**
     * 券名称
     */
    private String name;

    /**
     * 使用品类,0全部,1家具,2建材
     */
    private Integer category;

    /**
     * 券有效期开始时间
     */
    private Date startTime;

    /**
     * 券有效期结束时间
     */
    private Date endTime;

    /**
     * 单次派发数量
     */
    private Integer singleNum;

    /**
     * 操作人，用户PID
     */
    private String operator;


    /**
     *  全部参数构造
     */
    public ActiVouc(Long id, String pid, String activityPid, String voucherType, String name, Integer category, Date startTime, Date endTime, Integer singleNum, Date createTime, Date updateTime, String operator, Integer status, Integer version){
        this.id = id;
        this.pid = pid;
        this.activityPid = activityPid;
        this.voucherType = voucherType;
        this.name = name;
        this.category = category;
        this.startTime = startTime;
        this.endTime = endTime;
        this.singleNum = singleNum;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.operator = operator;
        this.status = status;
        this.version = version;
    }
    /**
     *  默认构造
     */
    public ActiVouc() {}



    public String getActivityPid() {
        return activityPid;
    }

    public void setActivityPid(String activityPid) {
        this.activityPid = activityPid;
    }


    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public Integer getSingleNum() {
        return singleNum;
    }

    public void setSingleNum(Integer singleNum) {
        this.singleNum = singleNum;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }



    @Override
    public String toString() {
        return "ActiVouc{" +
           "id=" + id +
              ", pid=" + pid +
              ", activityPid=" + activityPid +
              ", voucherType=" + voucherType +
              ", name=" + name +
              ", category=" + category +
              ", startTime=" + startTime +
              ", endTime=" + endTime +
              ", singleNum=" + singleNum +
              ", createTime=" + createTime +
              ", updateTime=" + updateTime +
              ", operator=" + operator +
              ", status=" + status +
              ", version=" + version +
       '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        ActiVouc model = (ActiVouc) o;

        return
            Objects.equal(id, model.id)
                && Objects.equal(pid, model.pid)
                && Objects.equal(activityPid, model.activityPid)
                && Objects.equal(voucherType, model.voucherType)
                && Objects.equal(name, model.name)
                && Objects.equal(category, model.category)
                && Objects.equal(startTime, model.startTime)
                && Objects.equal(endTime, model.endTime)
                && Objects.equal(singleNum, model.singleNum)
                && Objects.equal(createTime, model.createTime)
                && Objects.equal(updateTime, model.updateTime)
                && Objects.equal(operator, model.operator)
                && Objects.equal(status, model.status)
                && Objects.equal(version, model.version)
    ;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pid, activityPid, voucherType, name, category, startTime, endTime, singleNum, createTime, updateTime, operator, status, version);
    }


}
