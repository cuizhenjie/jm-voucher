package com.jimei.voucher.pojo;


import com.google.common.base.Objects;
import com.jimei.common.modules.BaseModel;
import com.jimei.voucher.vo.ActivityVO;


import java.util.Date;

/**
 * 
 * 
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 10:43:09
 */
public class Activity extends BaseModel<ActivityVO> {

	private static final long serialVersionUID = 1L;


    /**
     *活动名称
     */
    private String name;

    /**
     *图片,取banner第一张
     */
    private String img;

    /**
     *banner,','分隔
     */
    private String banner;

    /**
     *参与门店
     */
    private String shop;

    /**
     *价格(分)
     */
    private Integer amount;

    /**
     *活动类型 1正常2不需要调用erp3不需要支付
     */
    private Integer type;

    /**
     *券类型,1代金券;2储值卡;3资格券
     */
    private Integer voucherMode;

    /**
     *总派发套数
     */
    private Integer count;

    /**
     *是否可重复购买0不可1可
     */
    private Integer rebuy;

    /**
     *活动开始时间
     */
    private Date startTime;

    /**
     *活动结束时间
     */
    private Date endTime;


    /**
     *操作人，用户PID
     */
    private String operator;

    /**
     *排序,默认0
     */
    private Integer sort;


    /**
     *活动介绍
     */
    private String intro;


    /**
     *  全部参数构造
     */
    public Activity(Long id, String pid, String name, String img, String banner, String intro, String shop, Integer amount, Integer type, Integer voucherMode, Integer count, Integer rebuy, Date startTime, Date endTime, Date createTime, Date updateTime, String operator, Integer sort, Integer status, Integer version){
        this.id = id;
        this.pid = pid;
        this.name = name;
        this.img = img;
        this.banner = banner;
        this.intro = intro;
        this.shop = shop;
        this.amount = amount;
        this.type = type;
        this.voucherMode = voucherMode;
        this.count = count;
        this.rebuy = rebuy;
        this.startTime = startTime;
        this.endTime = endTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.operator = operator;
        this.sort = sort;
        this.status = status;
        this.version = version;
    }
    /**
     *  默认构造
     */
    public Activity() {}



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }


    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }


    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Integer getRebuy() {
        return rebuy;
    }

    public void setRebuy(Integer rebuy) {
        this.rebuy = rebuy;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getVoucherMode() {
        return voucherMode;
    }

    public void setVoucherMode(Integer voucherMode) {
        this.voucherMode = voucherMode;
    }

    @Override
    public String toString() {
        return "Activity{" +
           "id=" + id +
              ", pid=" + pid +
              ", name=" + name +
              ", img=" + img +
              ", banner=" + banner +
              ", intro=" + intro +
              ", shop=" + shop +
              ", amount=" + amount +
              ", type=" + type +
              ", voucherMode=" + voucherMode +
              ", count=" + count +
              ", rebuy=" + rebuy +
              ", startTime=" + startTime +
              ", endTime=" + endTime +
              ", createTime=" + createTime +
              ", updateTime=" + updateTime +
              ", operator=" + operator +
              ", sort=" + sort +
              ", status=" + status +
              ", version=" + version +
       '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Activity model = (Activity) o;

        return
            Objects.equal(id, model.id)
                && Objects.equal(pid, model.pid)
                && Objects.equal(name, model.name)
                && Objects.equal(img, model.img)
                && Objects.equal(banner, model.banner)
                && Objects.equal(intro, model.intro)
                && Objects.equal(shop, model.shop)
                && Objects.equal(amount, model.amount)
                && Objects.equal(type, model.type)
                && Objects.equal(voucherMode, model.voucherMode)
                && Objects.equal(count, model.count)
                && Objects.equal(rebuy, model.rebuy)
                && Objects.equal(startTime, model.startTime)
                && Objects.equal(endTime, model.endTime)
                && Objects.equal(createTime, model.createTime)
                && Objects.equal(updateTime, model.updateTime)
                && Objects.equal(operator, model.operator)
                && Objects.equal(sort, model.sort)
                && Objects.equal(status, model.status)
                && Objects.equal(version, model.version)
    ;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pid, name, img, banner, intro, shop, amount, type, count, voucherMode, rebuy, startTime, endTime, createTime, updateTime, operator, sort, status, version);
    }


}
