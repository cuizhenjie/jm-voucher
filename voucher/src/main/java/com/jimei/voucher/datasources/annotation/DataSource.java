package com.jimei.voucher.datasources.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解
 * @author czj
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}
