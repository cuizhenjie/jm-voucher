package com.jimei.voucher.datasources;

/**
 * 增加多数据源，在此配置
 *
 * @author czj
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";

}
