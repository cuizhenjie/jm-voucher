package com.jimei.voucher.service;


import java.util.List;

import com.jimei.common.ServerResponse;
import com.jimei.voucher.vo.ActiListVO;

/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
public interface ActiListService {

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    ServerResponse<ActiListVO> selectByPid(String pid);

    /**
    * 新增方法
    * @return
    */
    ServerResponse<String> add(ActiListVO actiListVo);

    /**
    * 修改方法
    * @return
    */
    ServerResponse<String> update(ActiListVO actiListVo);


    /**
    * 批量逻辑删除方法
    * @param pids
    * @return
    */
    ServerResponse<String> deleteByPids(List<String> pids);

    /**
     * 获取活动场次列表
     * @param params
     * @return
     */
    List<ActiListVO> selectActivityList(String activityPid);

    /**
     * 获取活动场次列表(封装response)
     * @param activityPid
     * @return
     */
    ServerResponse<List<ActiListVO>> selectActiListResp(String activityPid);
}

