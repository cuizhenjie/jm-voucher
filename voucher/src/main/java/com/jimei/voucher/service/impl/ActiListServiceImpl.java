package com.jimei.voucher.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.SnowflakeIdWorker;
import com.jimei.voucher.dao.ActiListMapper;
import com.jimei.voucher.exception.VoucherException;
import com.jimei.voucher.pojo.ActiList;
import com.jimei.voucher.service.ActiListService;
import com.jimei.voucher.vo.ActiListVO;


/**
 *
 * Service实现类
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
@Service("actiListService")
public class ActiListServiceImpl implements ActiListService {
//    private static Logger logger = LoggerFactory.getLogger(ActiListServiceImpl.class.getSimpleName());

    @Autowired
    private ActiListMapper actiListMapper;

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    @Override
    public ServerResponse<ActiListVO> selectByPid(String pid) {
        ActiList actiList = actiListMapper.selectByPid(pid);
        if(actiList == null){
            return ServerResponse.createByErrorMessage("请确认查询参数是否正确");
        }
        ActiListVO vo = new ActiListVO();
        actiList.toVO(vo);
        return ServerResponse.createBySuccess(vo);
    }

    @Override
    public List<ActiListVO> selectActivityList(String activityPid) {
        return actiListMapper.selectActivityList(activityPid);
    }

    @Override
    public ServerResponse<List<ActiListVO>> selectActiListResp(String activityPid) {
        List<ActiListVO> vos = selectActivityList(activityPid);
        return ServerResponse.createBySuccess(vos);
    }


    /**
     * 新增方法
     * @return
     */
    @Override
    public ServerResponse<String> add(ActiListVO actiListVo) {
        ActiList actiList = new ActiList();
        actiList.fromVO(actiListVo);
        //设置pid
        actiList.setPid(SnowflakeIdWorker.getInstance(0,0).nextId() + "");
        actiList.setStatus(0);

        int addRowCount = actiListMapper.insertSelective(actiList);
        if(addRowCount <= 0){
            throw new VoucherException("新增数据失败");
        }
        return ServerResponse.createBySuccessMessage("新增数据成功");
    }


    /**
     * 修改方法
     * @return
     */
    @Override
    public ServerResponse<String> update(ActiListVO actiListVo) {
        ActiList actiList = new ActiList();
        actiList.fromVO(actiListVo);

        int updateRowCount = actiListMapper.updateByPidSelective(actiList);
        if(updateRowCount <= 0){
            throw new VoucherException("修改数据失败");
        }
        return ServerResponse.createBySuccessMessage("修改数据成功");
    }


    /**
     * 删除方法
     * @return
     */
    @Override
    public ServerResponse<String> deleteByPids(List<String> pids) {
        int deleteRowCount = actiListMapper.deleteByPidSelective(pids);
        if(deleteRowCount <= 0){
            throw new VoucherException("删除数据失败");
        }
        return ServerResponse.createBySuccessMessage("删除数据成功");
    }
}
