package com.jimei.voucher.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.SnowflakeIdWorker;
import com.jimei.voucher.dao.ActiVoucMapper;
import com.jimei.voucher.exception.VoucherException;
import com.jimei.voucher.pojo.ActiVouc;
import com.jimei.voucher.service.ActiVoucService;
import com.jimei.voucher.vo.ActiVoucVO;


/**
 *
 * Service实现类
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
@Service("actiVoucService")
public class ActiVoucServiceImpl implements ActiVoucService {
//    private Logger logger = LoggerFactory.getLogger(ActiVoucServiceImpl.class.getSimpleName());

    @Resource
    private ActiVoucMapper actiVoucMapper;

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    @Override
    public ServerResponse<ActiVoucVO> selectByPid(String pid) {
        ActiVouc actiVouc = actiVoucMapper.selectByPid(pid);
        if(actiVouc == null){
            return ServerResponse.createByErrorMessage("请确认查询参数是否正确");
        }
        ActiVoucVO vo = new ActiVoucVO();
        actiVouc.toVO(vo);
        return ServerResponse.createBySuccess(vo);
    }


    /**
     * 按照条件获取数据
     * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
     * @param params
     * @return
     */
    @Override
    public ServerResponse<List<ActiVoucVO>> selectListByActivity(String activityPid) {
        List<ActiVoucVO> sourceList = actiVoucMapper.selectListByActivity(activityPid);
        return ServerResponse.createBySuccess(sourceList);
    }


    /**
     * 新增方法
     * @return
     */
    @Override
    public ServerResponse<String> add(ActiVoucVO actiVoucVo) {
        ActiVouc actiVouc = new ActiVouc();
        actiVouc.fromVO(actiVoucVo);
        //设置pid
        actiVouc.setPid(SnowflakeIdWorker.getInstance(0,0).nextId() + "");
        actiVouc.setStatus(0);
        int addRowCount = actiVoucMapper.insertSelective(actiVouc);
        if(addRowCount <= 0){
            throw new VoucherException("新增数据失败");
        }

        return ServerResponse.createBySuccessMessage("新增数据成功");
    }


    /**
     * 修改方法
     * @return
     */
    @Override
    public ServerResponse<String> update(ActiVoucVO actiVoucVo) {
        ActiVouc actiVouc = new ActiVouc();
        actiVouc.fromVO(actiVoucVo);

        int updateRowCount = actiVoucMapper.updateByPidSelective(actiVouc);
        if(updateRowCount <= 0){
            throw new VoucherException("修改数据失败");
        }
        return ServerResponse.createBySuccessMessage("修改数据成功");
    }


    /**
     * 删除方法
     * @return
     */
    @Override
    public ServerResponse<String> deleteByPids(List<String> pids) {
        int deleteRowCount = actiVoucMapper.deleteByPidSelective(pids);
        if(deleteRowCount <= 0){
            throw new VoucherException("删除数据失败");
        }
        return ServerResponse.createBySuccessMessage("删除数据成功");
    }




}
