package com.jimei.voucher.service;


import java.util.List;
import java.util.Map;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.vo.OrderVO;

/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
public interface OrderService {

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    ServerResponse<OrderVO> selectByPid(String pid);


    /**
    * 按照条件获取数据
    * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
    * @param params
    * @return
    */
    ServerResponse<PageBean<OrderVO>> selectListByParams(Map<String, Object> params);

    /**
    * 新增方法
    * @return
    */
    ServerResponse<String> add(OrderVO orderVo);

    /**
    * 修改方法
    * @return
    */
    int update(OrderVO orderVo);


    /**
    * 批量逻辑删除方法
    * @param pids
    * @return
    */
    ServerResponse<String> deleteByPids(List<String> pids);

    /**
     * 取消订单
     * @param pid
     */
    void cancelByPid(String pid) throws Exception;


}

