package com.jimei.voucher.service;

import com.jimei.common.ServerResponse;

import java.util.Map;

/**
 * 系统日志service
 * @author czj
 */
public interface SysLogService {

    /**
     * 查询数据
     * @param params
     * @return
     */
    ServerResponse selectListByParams(Map<String,Object> params);

}
