package com.jimei.voucher.service;


import java.util.List;
import java.util.Map;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.vo.ActivityVO;

/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
public interface ActivityService {

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    ServerResponse selectByPid(String pid);


    /**
    * 按照条件获取数据
    * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
    * @param params
    * @return
    */
    ServerResponse<PageBean<ActivityVO>> selectListByParams(Map<String, Object> params);

    /**
    * 新增方法
    * @return
    */
    ServerResponse add(ActivityVO activityVo);

    /**
    * 修改方法
    * @return
    */
    ServerResponse update(ActivityVO activityVo);

    /**
     * 置顶活动
     * @param pid
     * @return
     */
    ServerResponse updateActivityTop (String pid);

    /**
    * 批量逻辑删除方法
    * @param pids
    * @return
    */
    ServerResponse deleteByPids(List<String> pids);

    /**
     * 获取活动专题详情信息
     * @param id 活动pid
     * @return
     */
    ServerResponse<ActivityVO> selectDetail(String id);

}

