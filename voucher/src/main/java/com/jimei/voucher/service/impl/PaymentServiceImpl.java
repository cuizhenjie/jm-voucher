package com.jimei.voucher.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.jimei.common.ServerResponse;
import com.jimei.common.modules.Const;
import com.jimei.common.modules.PageBean;
import com.jimei.common.modules.SnowflakeIdWorker;
import com.jimei.voucher.dao.PaymentMapper;
import com.jimei.voucher.pojo.Order;
import com.jimei.voucher.pojo.Payment;
import com.jimei.voucher.service.PaymentService;
import com.jimei.voucher.vo.PaymentVO;


/**
 *
 * Service实现类
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
@Service("paymentService")
public class PaymentServiceImpl implements PaymentService {
    private static Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class.getSimpleName());

    @Resource
    private PaymentMapper paymentMapper;

    /**
     * 按照条件获取数据
     * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
     * @param params
     * @return
     */
    @Override
    public ServerResponse<PageBean<PaymentVO>> selectListByParams(Map<String,Object> params) {
        //设置分页条件
        int page = 0;
        int pageSize = Const.DEFAULT_PAGE_SIZE;
        Object pageParams = params.get("page");
        Object pageSizeParams = params.get("pageSize");

        if(pageParams != null){
            page = Integer.valueOf(pageParams.toString());
        }
        if(pageSizeParams != null){
            pageSize = Integer.valueOf(pageSizeParams.toString());
        }
        PageHelper.startPage(page, pageSize);
        List<PaymentVO> sourceList = paymentMapper.selectListByParams(params);
        PageBean<PaymentVO> pageResult = new PageBean<>(sourceList);

        return ServerResponse.createBySuccess(pageResult);
    }

    /**
     * 发起支付接口调用新增支付流水
     * @return
     */
    @Override
    public int add(Order order) {
        logger.info("add {}", order);
        Payment payment = new Payment();
        payment.setPid(SnowflakeIdWorker.getInstance(0,0).nextId() + "");//设置pid
        payment.setOrderPid(order.getPid());
        payment.setCreateTime(order.getCreateTime());
        payment.setUpdateTime(order.getCreateTime());
        payment.setDealCode(order.getDealCode());
        payment.setPayAmount(order.getPayAmount());
        payment.setPayStatus(order.getOrderStatus());
        payment.setPayTime(order.getPayTime());
        payment.setPayType(order.getPayType());
        payment.setStatus(0);
        return paymentMapper.insertSelective(payment);
    }


    /**
     * 根据支付回调修改支付状态
     * @return
     */
    @Override
    public int updatePayStatus(String pid, Integer payStatus) {
        logger.info("updatePayStatus pid={};payStatus={}", pid, payStatus);
        Payment payment = new Payment();
        payment.setPid(pid);
        payment.setPayStatus(payStatus);
        return paymentMapper.updatePayStatusByPid(payment);
    }
}
