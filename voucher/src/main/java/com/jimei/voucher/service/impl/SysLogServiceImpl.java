package com.jimei.voucher.service.impl;

import com.jimei.common.ServerResponse;
import com.jimei.voucher.service.SysLogService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 系统日志service实现类
 * @author czj
 */
@Service("sysLogService")
public class SysLogServiceImpl implements SysLogService {

    /**
     * 获取列表数据
     * @param params
     * @return
     */
    @Override
    public ServerResponse selectListByParams(Map<String, Object> params) {
        return null;
    }
}
