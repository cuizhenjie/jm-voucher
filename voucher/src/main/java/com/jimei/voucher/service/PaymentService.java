package com.jimei.voucher.service;


import java.util.Map;

import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import com.jimei.voucher.pojo.Order;
import com.jimei.voucher.vo.PaymentVO;

/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
public interface PaymentService {

    /**
    * 按照条件获取数据
    * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
    * @param params
    * @return
    */
    ServerResponse<PageBean<PaymentVO>> selectListByParams(Map<String, Object> params);

    /**
    * 新增方法
    * @return
    */
    int add(Order order);

    /**
    * 修改方法
    * @return
    */
    int updatePayStatus(String pid, Integer payStatus);

}

