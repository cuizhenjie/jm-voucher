package com.jimei.voucher.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradeCloseRequest;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeCloseResponse;
import com.alipay.api.response.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.jimei.voucher.service.AliPayService;
/**
 * 支付宝支付实现类
 * 
 * 2018年9月14日
 */
@Service
public class AliPayServiceImpl implements AliPayService {
    private static Logger logger = LoggerFactory.getLogger(AliPayServiceImpl.class);
    
    /**
     * 支付宝服务器主动通知商户服务器
     */
    private String notifyUrl;
    
    /**
     * 同步返回地址,支付完成后跳转地址
     */
    private String returnUrl;
    
    @Autowired
    private AlipayClient alipayClient;

    @Override
    public String pagePay(String bizContent) throws Exception {
        logger.info("pagePay,{}", bizContent);
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        request.setNotifyUrl(notifyUrl);
        request.setReturnUrl(returnUrl);
        request.setBizContent(bizContent);
        AlipayTradePagePayResponse response = alipayClient.pageExecute(request);
        logger.info("pagePay,{}", response.getBody());
        if(!response.isSuccess()) {
            return null;
        }
        return response.getBody();
    }

    @Override
    public boolean refund(String bizContent) throws Exception {
        logger.info("refund,{}", bizContent);
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        request.setBizContent(bizContent);
        AlipayTradeRefundResponse response = alipayClient.execute(request);
        logger.info("refund,{}", response.getBody());
        return response.isSuccess();
    }

    @Override
    public AlipayTradeFastpayRefundQueryResponse refundQuery(String bizContent) throws Exception {
        logger.info("refundQuery,{}", bizContent);
        AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
        request.setBizContent(bizContent);
        AlipayTradeFastpayRefundQueryResponse response = alipayClient.execute(request);
        logger.info("refundQuery,{}", response.getBody());
        return response;
    }

    @Override
    public AlipayTradeQueryResponse query(String bizContent) throws Exception {
        logger.info("query,{}", bizContent);
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent(bizContent);
        AlipayTradeQueryResponse response = alipayClient.execute(request);
        logger.info("query,{}", response.getBody());
        return response;
    }

    @Override
    public boolean close(String bizContent) throws Exception {
        logger.info("close,{}", bizContent);
        AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
        request.setBizContent(bizContent);
        AlipayTradeCloseResponse response = alipayClient.execute(request);
        logger.info("close,{}", response.getBody());
        return response.isSuccess();
    }

}
