package com.jimei.voucher.service;


import com.jimei.common.ServerResponse;
import com.jimei.voucher.pojo.Voucher;
import com.jimei.voucher.vo.VoucherVO;

import java.util.Map;
import java.util.List;

/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
public interface VoucherService {

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    ServerResponse selectByPid(String pid);


    /**
    * 按照条件获取数据
    * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
    * @param params
    * @return
    */
    ServerResponse selectListByParams(Map<String, Object> params);

    /**
    * 新增方法
    * @return
    */
    ServerResponse add(VoucherVO voucherVo);

    /**
    * 修改方法
    * @return
    */
    ServerResponse update(VoucherVO voucherVo);


    /**
    * 批量逻辑删除方法
    * @param pids
    * @return
    */
    ServerResponse deleteByPids(List<String> pids);


}

