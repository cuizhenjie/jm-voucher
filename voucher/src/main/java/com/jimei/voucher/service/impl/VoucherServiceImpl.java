package com.jimei.voucher.service.impl;

import com.google.common.collect.Lists;
import com.github.pagehelper.PageHelper;
import com.jimei.common.modules.Const;
import com.jimei.common.modules.SnowflakeIdWorker;
import org.springframework.stereotype.Service;
import com.jimei.voucher.exception.VoucherException;
import java.util.Map;
import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.jimei.common.ServerResponse;
import com.jimei.common.modules.PageBean;
import org.springframework.util.StringUtils;

import com.jimei.voucher.dao.VoucherMapper;
import com.jimei.voucher.pojo.Voucher;
import com.jimei.voucher.vo.VoucherVO;
import com.jimei.voucher.service.VoucherService;


/**
 *
 * Service实现类
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
@Service("voucherService")
public class VoucherServiceImpl implements VoucherService {

    private Logger logger = LoggerFactory.getLogger(VoucherServiceImpl.class.getSimpleName());

    @Resource
    private VoucherMapper voucherMapper;

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    @Override
    public ServerResponse selectByPid(String pid) {

        Voucher voucher = voucherMapper.selectByPid(pid);
        if(voucher == null){
            return ServerResponse.createByErrorMessage("请确认查询参数是否正确");
        }

        return ServerResponse.createBySuccess(voucher);
    }


    /**
     * 按照条件获取数据
     * params中需包含sort（需要排序字段），order（排序方式 desc，asc等）
     * @param params
     * @return
     */
    @Override
    public ServerResponse selectListByParams(Map<String,Object> params) {

        //设置分页条件
        int page = 0;
        int pageSize = Const.DEFAULT_PAGE_SIZE;
        Object pageParams = params.get("page");
        Object pageSizeParams = params.get("pageSize");

        if(pageParams != null){
            page = Integer.valueOf(pageParams.toString());
        }
        if(pageSizeParams != null){
            pageSize = Integer.valueOf(pageSizeParams.toString());
        }

        PageHelper.startPage(page, pageSize);
        List<Voucher> sourceList = voucherMapper.selectListByParams(params);

        //分页数据封装
        List<VoucherVO> resultList = Lists.newArrayList();
        for(Voucher voucher : sourceList){
            resultList.add(voucher.toVO(null));
        }
        PageBean pageResult = new PageBean(sourceList);
        pageResult.setList(resultList);

        return ServerResponse.createBySuccess(pageResult);
    }


    /**
     * 新增方法
     * @return
     */
    @Override
    public ServerResponse add(VoucherVO voucherVo) {

        Voucher voucher = new Voucher();
        voucher.fromVO(voucherVo);
        //设置pid
        voucher.setPid(SnowflakeIdWorker.getInstance(0,0).nextId() + "");

        int addRowCount = voucherMapper.insertSelective(voucher);

        if(addRowCount <= 0){
            throw new VoucherException("新增数据失败");
        }

        return ServerResponse.createByErrorMessage("新增数据成功");
    }


    /**
     * 修改方法
     * @return
     */
    @Override
    public ServerResponse update(VoucherVO voucherVo) {

        Voucher voucher = new Voucher();
        voucher.fromVO(voucherVo);


        int updateRowCount = voucherMapper.updateByPidSelective(voucher);

        if(updateRowCount <= 0){
            throw new VoucherException("修改数据失败");
        }

        return ServerResponse.createByErrorMessage("修改数据成功");
    }


    /**
     * 删除方法
     * @return
     */
    @Override
    public ServerResponse deleteByPids(List<String> pids) {

        int deleteRowCount = voucherMapper.deleteByPidSelective(pids);

        if(deleteRowCount <= 0){
            throw new VoucherException("删除数据失败");
        }

        return ServerResponse.createByErrorMessage("删除数据成功");
    }




}
