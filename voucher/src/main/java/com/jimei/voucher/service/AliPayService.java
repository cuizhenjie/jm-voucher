package com.jimei.voucher.service;

import com.alipay.api.response.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;

/**
 * 支付宝支付
 * 
 * 2018年9月14日
 */
public interface AliPayService {
    
    /**
     * 统一收单下单并支付页面接口
     */
    String pagePay(String bizContent) throws Exception;
    
    /**
     * 统一收单交易退款接口
     */
    boolean refund(String bizContent) throws Exception;
    
    /**
     * 统一收单交易退款查询接口
     */
    AlipayTradeFastpayRefundQueryResponse refundQuery(String bizContent) throws Exception;
    
    /**
     * 统一收单线下交易查询接口
     */
    AlipayTradeQueryResponse query(String bizContent) throws Exception;
    
    /**
     * 统一收单交易关闭接口
     * @param bizContent 
     * @return
     * @throws Exception
     */
    boolean close(String bizContent) throws Exception;
}
