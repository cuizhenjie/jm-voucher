package com.jimei.voucher.service;


import java.util.List;

import com.jimei.common.ServerResponse;
import com.jimei.voucher.vo.ActiVoucVO;

/**
 * 
 *
 * @author jimei
 * @email 10832053@qq.com
 * @date 2018-09-07 11:02:58
 */
public interface ActiVoucService {

    /**
    * 通过pid获取对象
    * @param pid
    * @return
    */
    ServerResponse<ActiVoucVO> selectByPid(String pid);


    /**
    * 按照条件获取数据
    * @param activityPid
    * @return
    */
    ServerResponse<List<ActiVoucVO>> selectListByActivity(String activityPid);

    /**
    * 新增方法
    * @return
    */
    ServerResponse<String> add(ActiVoucVO actiVoucVo);

    /**
    * 修改方法
    * @return
    */
    ServerResponse<String> update(ActiVoucVO actiVoucVo);


    /**
    * 批量逻辑删除方法
    * @param pids
    * @return
    */
    ServerResponse<String> deleteByPids(List<String> pids);


}

