# jm-voucher
## 前言

　　`jm-voucher`是新版本实现券业务逻辑，兼容单次抢购单张及多张套券形式

## 项目介绍

　　基于SpringBoot+Mybatis系统架构，提供公共微服务服务模块：。

### 组织结构

``` lua
jm-voucher
├── voucher-common -- 框架公共模块
├── voucher-upms -- 用户权限管理系统
└── voucher-demo -- 示例模块(包含一些示例代码等)
     └── voucher-demo-web -- 演示
```

### 技术选型


#### 模块介绍

> voucher-common

SpringBoot+Mybatis框架集成公共模块，包括公共配置、MybatisGenerator扩展插件、通用BaseService、工具类等。
